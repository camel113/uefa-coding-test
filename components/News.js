import React, {Component} from 'react';
import {View, Text, StyleSheet, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import Article from './Article'
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';


export default class News extends Component {

  constructor(props){
    super(props);
    this.state = {
      news: [],
      newsIndex: 0,
      isLoading: true,
      error: false
    }
  }


  componentDidMount(){
    this.getArticlesfromApi()
  }

  onSwipeLeft(gestureState) {
    if(this.state.newsIndex > 0){this.setState({newsIndex:this.state.newsIndex-1})}
  }

  onSwipeRight(gestureState) {
    if(this.state.newsIndex < this.state.news.length-1){this.setState({newsIndex:this.state.newsIndex+1})}
  }

  async getArticlesfromApi(){

    const fetchUrl = 'http://www.mocky.io/v2/5c6aa7fb330000652d7f4c2f';

    try{
      const response = await fetch(fetchUrl);
      const responseJson = await response.json();
      console.log(responseJson)
      this.setState({news:responseJson,isLoading:false})
    }catch(err){
      console.log(err)
      this.setState({isLoading:false,error:true})
    }

  }

  render(){

    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80
    };
    const {news, newsIndex, isLoading, error} = this.state;

    return(
      <GestureRecognizer
        onSwipeLeft={(state) => this.onSwipeLeft(state)}
        onSwipeRight={(state) => this.onSwipeRight(state)}
        config={config}
        style={{flex: 1}}>
        {!isLoading ? (
            !error ? (
              news.length > 0 ? (
                <Article title={news[newsIndex].overview.title} date={news[newsIndex].overview.publicationDate} imageUrl={news[newsIndex].overview.imageUrl} details={news[newsIndex].details.description}/>
              ):(<Text>No content</Text>)
            ):(
              <Text>Error</Text>
            )
          ):(
            <Text>isLoading...</Text>
          )
        }
      </GestureRecognizer>
    )
  }
}