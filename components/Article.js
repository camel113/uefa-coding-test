import React, {Component} from 'react';
import {View, Text, Image, StyleSheet, TouchableHighlight, ScrollView, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment'


export default class Article extends Component {

  constructor(props){
    super(props);
    this.state = {
      isScrollEnabled: false
    }
    this.screenHeight = Dimensions.get("window").height;
  }

  scrollToContent(){
    this.setState({isScrollEnabled:true})
    this.scroller.scrollTo({x: 0, y: 200});
  };

  render(){
    return(
      
      <ScrollView style={styles.scrollContainer} ref={(scroller) => {this.scroller = scroller}} scrollEnabled={this.state.isScrollEnabled}>
        <View style={[styles.container,{height:this.screenHeight}]}>
          <View style={styles.header}>
            <Text style={styles.headerText}>{this.props.title}</Text>
            <Text style={styles.headerText}>{moment(this.props.date).fromNow()}</Text>
          </View>
          <View style={{flex:1}}>
            <Image style={styles.image} source={{uri:this.props.imageUrl}}/>
          </View>
          <View style={styles.footerContainer}>
            <TouchableHighlight
              onPress={() => {
                this.scrollToContent()
              }}>
              <Text style={styles.headerText}>More info</Text>
            </TouchableHighlight>
          </View>
        </View>
        <View style={styles.detailsContainer}><Text>{this.props.details}</Text></View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  scrollContainer:{
    flex:1
  },
  container:{
    flex:1
  },
  image:{
    width: "100%",
    height: "100%",
    resizeMode: 'cover'
  },
  header:{
    alignItems:"center",
    paddingTop:50,
    height:100
  },
  headerText:{
    color:"#000",
    fontSize:16
  },
  footerContainer:{
    alignItems:"flex-end",
    position:"absolute",
    bottom:50,
    right:10
  },
  detailsContainer:{
    paddingTop:50, 
    paddingBottom: 50
  }
});

Article.propTypes = {
  title: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  details: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired
}